package usc.code;

public class MySingle {

	private static MySingle instance;
	
	public MySingle() {
		
	}
	
	public static MySingle getInstance() {
		if (instance == null) {
			synchronized(MySingle.class){
				if(instance == null){
					instance = new MySingle();
				}
			}
		}
		return instance;
	}
	
	public static void main(String[] args) {
		MySingle s1 = getInstance();
		MySingle s2 = getInstance();
		System.out.print(s1 == s2);
	}

}
